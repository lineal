#!/bin/bash

# Default Values
LNAME="sbcl" # Lisp name
MODE="shell" # Mode in which to run Lineal

LISP_NAMES=( "clisp" "cmucl" "ecl" "sbcl" )

usage()
{
    cat << EOF
    usage: ./run.sh [options]

    OPTIONS:
    -h      Show this message
    -l      Lisp name (${LISP_NAMES[@]})
    -m      Mode (shell, webui)

    (edit script to change default values)
EOF
}

while getopts "hl:m:" OPTION
do
    case $OPTION in
        h) usage
        exit 0 ;;
        l) LNAME="$OPTARG" ;;
        m) MODE="$OPTARG" ;;
        ?) usage
        exit ;;
    esac
done

LFILE="src/${MODE}/devvars.lisp"

case "$LNAME" in
    'clisp') clisp --quiet     -i    $LFILE ;;
    'cmucl') cmucl  -quiet     -load $LFILE ;;
    'ecl')     ecl             -load $LFILE ;;
    'sbcl')   sbcl --noinform --load $LFILE ;;
    *)
    echo "$LNAME  is not one of:  ${LISP_NAMES[@]}"
    exit 1
    ;;
esac

exit

