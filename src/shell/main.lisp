
(defpackage :lineal.shell
  (:use :cl :lineal :lineal.overload))
(in-package :lineal.shell)

(defun greeting (infixp)
  (format t "~&Welcome to Lineal, ")
  (format t "using ~:[prefix~;infix~] notation." infixp)
  (format t "~%To leave, use exit or quit.")
  (format t "~%> ")
  (force-output))

;;; Main loop for shell.
(defun shell-repl ()
  (let ((infixp (symbol-value 
                  'lineal.devvars::use-infix-p)))
    (greeting infixp)
    (handler-case
      (do ((str (read-line) (read-line)))
        ((or (string= "quit" str)
             (string= "exit" str)))
        (over-format
          (process-input-from-string
            str infixp)
          *standard-output*)
        (format t "~&> ")
        (force-output)) 
      (end-of-file (condit)
                   (declare (ignore condit))))))

