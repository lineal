
;;; Load main dev vars
(load (make-pathname :directory '(:relative "src")
                     :name "devvars" :type "lisp"))

(in-package :lineal.devvars)

;;; Load user preferences for shell Lineal.
(load (make-pathname :directory '(:relative "src")
                     :name "config" :type "lisp"))

(unless (boundp 'use-infix-p)
  (set 'use-infix-p t))

(setq *file-tree* 
      (nconc *file-tree*
             '(("shell" "main"))))

(compile-lineal)

;;; Load up restore file if desired.
(when (boundp 'restore-from-file)
  (let ((file (symbol-value 'restore-from-file)))
    (unless (pathnamep file)
      (setq file (make-pathname :name "captured_session")))
    (format t "Restoring session from: \"~A\"...~%"
            (namestring file))
    (princ (lineal::local-restore file))
    (fresh-line)))

(lineal.shell::shell-repl)

