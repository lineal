
(defun fail (arg)
  (format nil "What is this \"~A\" of which you speak?~%" arg))

(defgeneric expr-parse (expr))

(defmethod expr-parse ((expr list))
  (unless (boundp (car expr))
    (throw 'wtfvar (fail (symbol-name (car expr)))))
  (loop
    :for x :in (cdr expr)
    :collect (expr-parse x) :into to-eval
    :finally (return (cons (symbol-value (car expr))
                           to-eval))))

(defmethod expr-parse ((expr symbol))
  (if (boundp expr) (symbol-value expr)
    (throw 'wtfvar (fail (symbol-name expr)))))

(defmethod expr-parse ((expr number)) expr)
(defmethod expr-parse ((expr string)) expr)

(defun eval-parsed (s-exprs)
  (handler-case
    (catch 'over-ex (eval s-exprs))
    (error (conditn)
           (declare (ignore conditn))
           (format nil "Something went wrong during eval, ~
                   you probably used a function with bad parameters."))))

(defun process-prefix-from-stream (strm)
  (let ((*readtable* (copy-readtable)))
    (setf (readtable-case *readtable*) :preserve)
    (handler-case
      (catch
        'wtfvar
        (do (expr result)
          (nil)
          (let ((*package*
                  (find-package :lineal.client-vars)))
            (setq expr (read strm nil nil)))
          (unless expr (return result))
          (setq result (eval-parsed (expr-parse expr)))))
      (error (conditn)
             (declare (ignore conditn))
             "You probably forgot to add a closing parenthesis."))))

