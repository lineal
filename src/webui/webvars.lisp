
; note: for parenscript, may need to use #'escape-string
; http://www.groupsrv.com/computers/about410678.html
(defpackage :lineal.webui
  (:use :cl :cl-who :hunchentoot :parenscript
        :lineal.devvars :lineal :lineal.overload)
  (:export :start-server :with-html))

(in-package :lineal.webui)

(setf
  *dispatch-table*
  `(; index.lisp
    ,(create-regex-dispatcher "^/$" 'front-page)
    ,(create-prefix-dispatcher
       "/recall_vrbl" 'web-recall-vrbl)
    ; matrixui.lisp
    ,(create-prefix-dispatcher
       "/post_matrix_input" 'get-matrix-input)
    ,(create-prefix-dispatcher
       "/matrix_request" 'matrix-request)
    ,(create-prefix-dispatcher
       "/input_matrix" 'matrix-input-code)
    ,(create-prefix-dispatcher
       "/matrix_select_contents" 'matrix-select-contents)
    ; calcupage.lisp
    ,(create-prefix-dispatcher
       "/calcupage-infix" 'calcupage)
    ,(create-prefix-dispatcher
       "/calcupage-prefix" 'calcupage-prefix)
    ,(create-prefix-dispatcher
       "/process-infix" 'receive-and-process)
    ,(create-prefix-dispatcher
       "/process-prefix" 'receive-and-process-prefix)
    ; save-restore.lisp
    ,@(unless (boundp 'lineal.devvars::no-save-restore)
        (format t "Save/restore enabled!~%")
        (list
          (create-prefix-dispatcher
            "/save_restore" 'save-restore-page)
          (create-prefix-dispatcher
            "/capture_session" 'capture-session)
          (create-prefix-dispatcher
            "/restore_from_upload" 'restore-from-upload)))
    ; reload.lisp
    ,@(when (boundp 'lineal.devvars::reload-page)
        (format t "Reload page enabled!~%")
        (list (create-prefix-dispatcher "/reload" 'reload)))
    ;V literal content V
    ,(create-static-file-dispatcher-and-handler
       "/INSTALL.txt" "INSTALL" "text/plain")
    ,(create-static-file-dispatcher-and-handler
       "/COPYING.txt" "COPYING" "text/plain")
    ,(create-folder-dispatcher-and-handler
       "/doc/" (make-pathname :directory '(:relative "doc")))
    ,(create-folder-dispatcher-and-handler
       "/" (make-pathname
             :directory '(:relative "http_root")))))

