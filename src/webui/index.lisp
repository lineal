
;;; Insert javascript tags.
(defmacro js-tag (s . body)
  `(progn
     (princ "<script type=\"text/javascript\">" ,s)
     ,@body
     (princ "</script>" ,s)))

;;; Easily insert parenscript.
(defmacro ps-tag (s . body)
  `(js-tag ,s (princ (ps ,@body) ,s)))

(defmacro jsfile (s file)
  `(progn
     (princ "<script type=\"text/javascript\" src=\"/jsfiles/" ,s)
     (princ ,file ,s)
     (princ ".js\"></script>" ,s)))

;;; URL: /recall_vrbl
;;; Get a variable out of memory.
(defun web-recall-vrbl ()
  (recall-vrbl (parameter "name"))
  "varible wiped")

(defun calcupage-link (strm)
  (with-html-output
    (strm)
    (:a :href "/calcupage-infix" "Calculate things")
    " the regular way, or in "
    (:a :href "/calcupage-prefix" "lisp style") "."))

;;; URL: /
;;; The main page.
(defun front-page ()
  (with-html-output-to-string
    (s nil :prologue t)
    (:html
      (:head (:title "Welcome to Lineal v0.1.7"))
      (:body
        (:form 
          :name "optionForm"
          :action (ps-inline
                    (setf window.location
                          document.option-form.action-menu.value))
          (:div
            (:a :href "/input_matrix" "Create and edit")
            " vectors and matrices" :br
            (calcupage-link s) :br
            (unless (boundp 'lineal.devvars::no-save-restore)
              (with-html-output
                (s) (:a :href "/save_restore" "Save/Restore") " Session" :br))
            "Browse the "
            (:a :href "/doc/index.html" "documentation")
            " (you really should)" :br
            (when (boundp 'lineal.devvars::reload-page)
              (with-html-output
                (s) (:a :href "/reload" "Reload for changes") :br))
            :br (:a :href "/COPYING.txt"
                    "This software is subject to the MIT-style License")))))))

