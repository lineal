
(defvar name-field)
(defvar name-select)
(defvar row-field)
(defvar col-field)
(defvar rows)
(defvar cols)
(defvar mat)
(defvar out-div)

(defvar field-div)

;V When the drop-down list containing  V
;V matrix names gets its index changed.V
(defun select-name-change ()
  (if (eql name-select.selected-index 0)
    (progn
      (setf name-field.value ""
            rows 2 cols 2
            row-field.value 2
            col-field.value 2)
      (disp-fields
        (make-array (make-array 0 0)
                    (make-array 0 0))))
    (progn (setf name-field.value
                 (slot-value
                   (aref name-select.options
                         name-select.selected-index)
                   'text))
           (request-matrix))))

(defun disp-fields (matrix)
  "Display matrix cell fields"
  (defvar tmp-tr) (defvar tmp-td)
  (setf mat (make-array rows)
        field-div.inner-h-t-m-l "")
  (dotimes (i rows)
    (setf tmp-tr (.create-element document "tr")
          (aref mat i) (make-array cols))
    (.append-child field-div tmp-tr)
    (dotimes (j cols)
      (setf
        (aref mat i j) (.create-element document "input")
        (slot-value (aref mat i j) 'value)
        (if matrix (aref matrix i j) 0)
        tmp-td (.create-element document "td"))
      (.set-attribute (aref mat i j) "onFocus" "select()")
      (.append-child tmp-tr tmp-td)
      (.append-child tmp-td (aref mat i j)))))

(defun reset-fields ()
  "Reset matrix cell fields"
  (setf field-div.inner-h-t-m-l ""
        rows row-field.value
        cols col-field.value)
  (disp-fields))

(defun send-matrix ()
  (defvar name name-field.value)
  (defvar params
    (+ "name=" name
       "&rows=" rows
       "&cols=" cols
       "&matrix=("))
  (dotimes (i rows)
    (setf params (+ params "("))
    (dotimes (j cols)
      (setf params (+ params (slot-value (aref mat i j) 'value) " ")))
    (setf params (+ params ")")))
  (setf params (+ params ")"))
  (setf out-div.inner-h-t-m-l params)
  (post-params
    "/post_matrix_input" params
    (lambda (request)
      (setf name-select.inner-h-t-m-l request
            out-div.inner-h-t-m-l
            (+ out-div.inner-h-t-m-l
               "<br/>Value of " name " has been updated."))
      (dotimes (i name-select.length)
        (when (= name (slot-value (aref name-select i) 'text))
          (setf name-select.selected-index i))))))

(defun request-matrix ()
  (post-params
    "/matrix_request" (+ "name=" name-field.value)
    (lambda (response)
      (defvar matrix)
      (eval response)
      (setf row-field.value rows
            col-field.value cols)
      (disp-fields matrix))))

;V Recall a variable from server's memory.V
(defun recall-matrix ()
  (post-params
    "/recall_vrbl" (+ "name=" name-field.value)
    (lambda (response)
      (post-params
        "/matrix_select_contents" ""
        (lambda (response)
          (setf name-select.selected-index 0)
          (setf name-select.inner-h-t-m-l response))))))

