
(defun post-params (loc params fn)
  (let ((xmlhttp (new (*x-m-l-http-request))))
    ;initiate post request
    (.open xmlhttp "POST" loc t)
    (.set-request-header xmlhttp "Content-type" "application/x-www-form-urlencoded")
    (.set-request-header xmlhttp "Content-length" params.length)
    (.set-request-header xmlhttp "Connection" "close")
    ;V Send the params.V
    (.send xmlhttp params)
    (when fn
      (setf xmlhttp.onreadystatechange
            (lambda ()
              (when (and (= xmlhttp.ready-state 4)
                         (= xmlhttp.status 200))
                (fn xmlhttp.response-text)))))))

