
(defvar calc-in)

;V Insert text into /calc-in/ at cursor and give it focus.V
(defun uinput (str)
  (defvar curpos (+ calc-in.selection-start str.length))
  (setf
    calc-in.value
    (+ (.substring calc-in.value 0 calc-in.selection-start)
       str (.substring calc-in.value calc-in.selection-end)))
  (setf calc-in.selection-start curpos
        calc-in.selection-end curpos)
  (.focus calc-in))

;V Called when the "clear" button is pressed.V
(defun cleartext ()
  (setf calc-in.value "")
  (.focus calc-in))

;V Called when the "OK" button is pressed.V
(defun send-calc ()
  (post-params
    process-loc
    (+ "text=" (.replace calc-in.value (regex "/\\+/g") "%2B"))
    (lambda (response)
      (setf (slot-value (.get-element-by-id document "resultPre")
                        'inner-h-t-m-l)
            response)
      (.focus calc-in))))


