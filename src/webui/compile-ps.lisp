
;V Compile given ParenScript file.V
(defun compile-ps (ps-path js-path)
  (when (and (probe-file js-path)
             (< (file-write-date ps-path)
                (file-write-date js-path)))
    (return-from compile-ps))
  (format t "Generating: ~A~%" (namestring js-path))
  (with-open-file (ps-strm ps-path)
    (with-open-file (js-strm js-path :direction :output
                             :if-exists :supersede)
      (loop :for lis = (read ps-strm nil nil)
            :while lis
            :do (princ (eval (list 'ps lis)) js-strm)
            :do (terpri js-strm)))))

(defun recompile-ps ()
  (ensure-directories-exist
    (make-pathname
      :directory '(:relative "http_root" "jsfiles")))
  (mapc
    (lambda (name)
      (compile-ps
        (make-pathname
          :directory '(:relative "src" "webui" "psfiles")
          :name name :type "lisp")
        (make-pathname
          :directory '(:relative "http_root" "jsfiles")
          :name name :type "js")))
    '("calcupage"
      "connects"
      "matrix-edit")))


