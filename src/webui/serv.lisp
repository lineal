
;;; In the  :lineal.webui  package.

(format t "serv.lisp is loading...~%")

(format t "Using ASDF, you better have it loaded already...~%")
(use-package :asdf)
(format t "... looks like it worked.~%")

(format t "Loading package dependencies.~%")

;V Load the required libraries using V
;V functions best with the lisp.     V
(mapcar
  (lambda (pkg)
    (format t "Loading ~A...~%" pkg)
    #+(or :sbcl :mcl :ecl :cmucl)
    (require pkg)
    #-(or :sbcl :mcl :ecl :cmucl)
    (asdf:operate 'asdf:load-op pkg)
    (format t "... ~A loaded.~%" pkg))
  (list :hunchentoot :cl-who :parenscript))

(format t "Compiling and loading source tree...~%")
;V Compile and Load V
(compile-lineal :compile-all (boundp 'compile-all))

(format t "Generating JavaScript files if needed...~%")
(lineal.webui::recompile-ps)
(format t "... success! We are now w3c complaint!~%")

;V Enact any config options.V
(when (boundp 'tmp-directory)
  (ensure-directories-exist
    (symbol-value 'tmp-directory))
  (setf lineal.webui::*tmp-directory*
        (symbol-value 'tmp-directory))
  (format t "Custom tmp directory set to: ~A~%"
          (truename (symbol-value 'tmp-directory))))

(when (boundp 'log-file)
  (open (ensure-directories-exist
          (symbol-value 'log-file))
        :direction :probe :if-does-not-exist :create)
  (setf (lineal.webui::log-file)
        (symbol-value 'log-file))
  (format t "Custom log file will be at: ~A~%"
          (truename (symbol-value 'log-file))))

;V Load up restore file.V
(when (boundp 'restore-from-file)
  (let ((file (symbol-value 'restore-from-file)))
    (unless (pathnamep file)
      (setq file (make-pathname :name "captured_session")))
    (format t "Restoring session from: \"~A\"...~%"
            (namestring file))
    (princ (lineal::local-restore file))
    (fresh-line)))

;V Start server at specified port. V
(let ((the-port (if (boundp 'port)
                  (symbol-value 'port) 41938)))
  (format t "Starting Lineal on port ~A... " the-port)
  (lineal.webui:start-server :port the-port)
  (format t "OK~%Now point your bowser to ~
          http://localhost:~A/ and have at it!~%"
          the-port))

(import 'lineal::local-capture :cl-user)


