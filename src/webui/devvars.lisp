
;;; Load main dev vars
(load (make-pathname :directory '(:relative "src")
                     :name "devvars" :type "lisp"))

(in-package :lineal.devvars)

;;; Add webui-specific stuff to list of program files.
(setq
  *file-tree*
  (nconc *file-tree*
         '(("webui" "webvars";<  :lineal.webui
            :lineal.webui "compile-ps" "index"
            "matrixui" "calcupage-buttons" "calcupage"
            "save-restore" "reload"))))

;;; Set any user-defined preferences specific to the web server.
(load (make-pathname :directory '(:relative "src" "webui")
                     :name "config" :type "lisp"))

;;; Saddle up and start riding.
(load (make-pathname :directory '(:relative "src" "webui")
                     :name "serv" :type "lisp"))

