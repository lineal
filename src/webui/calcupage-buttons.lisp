
(defun htm-button
  (text id action)
  (with-html-output-to-string
    (strm)
    (:input :type "button"
            :id id
            :onclick action
            :value text)))

(defun htm-input-button (name id tosend)
  (htm-button
    name id
    (concatenate 'string "uinput(\"" tosend "\")")))

(defun opern-button
  (name infixp &key (id "fn1") (input name))
  (htm-input-button
    name id
    (if infixp input
      (concatenate
        'string "(" input " "))))

(defun htm-fnbut
  (name id infix multi-param-p
        &optional (fntext name))
  (htm-input-button
    name id
    (if infix
      (concatenate
        'string fntext
        (if multi-param-p "(" " "))
      (concatenate
        'string "(" fntext " "))))

(defun htm-linalg-buttons (infix)
  (let
    ((linalg-fnlis
       `("rref" "ref" "det"
         nil "transpose" "inverse"
         nil ("dot") ("cross")
         ("proj") ("orth")
         nil ("cat") ("vcat") ("crop") ("vcrop"))))
    (with-output-to-string (strm)
      (mapc
        (lambda (args)
          (princ
            (cond
              ((not args) "<br/>")
              ((stringp args)
               (htm-fnbut
                 args
                 (format nil "fn~A" (length args))
                 infix nil))
              (t (destructuring-bind
                   (name &optional id) args
                   (htm-fnbut
                     name 
                     (if id id
                       (format nil "fn~A" (length name)))
                     infix t))))
            strm))
        linalg-fnlis))))


; (show-a-button "ok" elem "sendCalc()" 25 "white" "green"))
;(show-a-button "clear" elem "cleartext()" 45 "white" "darkred"))

;-- BEGIN SPECIAL BUTTON CREATORS --

;V Use the button operation as a name.V
(defun simple-input-button (name &optional (id "fn1"))
  (htm-input-button name id name))

(defun clear-button ()
  (htm-button "clear" "clear" "cleartext()"))


(defun ok-button ()
  (htm-button "ok" "ok" "sendCalc()"))


(defun memory-buttons (infix)
  (concatenate
    'string
    (htm-input-button
      "recall" "recall" 
      (if infix "recall(\\\"" "(recall \\\""))
    (htm-input-button
      "store" "store"
      (if infix "store(\\\"" "(store \\\""))))

;-- END SPECIAL BUTTON CREATORS --

(defun htm-standard-math-buttons (infixp)
  (labels
    ((htm-3digits
       (x)
       (with-output-to-string (strm)
         (dotimes (i 3)
           (princ
             (simple-input-button
               (princ-to-string (+ i x))
               "digit")
             strm)))))
    (concatenate
      'string
      (if infixp
        (opern-button "!" t)
        (opern-button "!" nil :input "factorial"))
      "<br />"
      (htm-fnbut "&radic;" "fn1" infixp nil "sqrt")
      (if infixp
        (opern-button "^" t)
        (opern-button "^" nil :input "expt"))
      (simple-input-button "(")
      (simple-input-button ")")
      "<br />" (htm-3digits 7)
      (opern-button "/" infixp)
      "<br />" (htm-3digits 4)
      (opern-button "&times;" infixp :input "*")
      "<br />" (htm-3digits 1)
      (opern-button "-" infixp)
      "<br />"
      (simple-input-button "0" "zerodigit")
      (simple-input-button "." "digit")
      (opern-button "+" infixp)
      "<br />"
      (memory-buttons infixp))))

