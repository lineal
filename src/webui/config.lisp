
;;;; See ../config.lisp

; Set Hunchentoot's temporary directory to
; a folder named "tmp" in Lineal's toplevel
; directory.
#+(or)
(set 'tmp-directory
     (make-pathname :directory '(:relative "tmp")))

; Make the log file's name "requests.log"
; and have it appear in Lineal's toplevel
; directory.
#+(or)
(set 'log-file
     (make-pathname :name "requests.log"))

; Log calculator input and replies.
#+(or) (set 'log-calcs t)

; Enable the reload page for easy changes.
#+(or) (set 'reload-page t)

; Disable save/restore.
#+(or) (set 'no-save-restore t)

; Set Hunchentoot listens to listen on port 8080
; instead of the default 41938
#+(or) (set 'port 8080)


