
;V URL: /process-infix
;V Receive user's calculation, return result.V
(defun receive-and-process
  (&optional (infixp t)
   &aux (text (parameter "text"))
        (log-calcs (load-time-value
                     (boundp 'lineal.devvars::log-calcs))))
  (when log-calcs (log-message* "parser receives: ~%~A" text))
  (setq
    text
    (with-output-to-string (strm)
      (over-format
        (process-input-from-string text infixp)
        strm)))
  (when log-calcs (log-message* "parser replies: ~%~A" text))
  text)

;V URL: /process-prefix
(defun receive-and-process-prefix ()
  (receive-and-process nil))

(defmacro htm-expand-known-if
  (strm infixp s-expr)
  `(princ
     (if ,infixp
       (load-time-value (,@s-expr t))
       (load-time-value (,@s-expr nil)))
     ,strm))

;V URL: /calcupage-infix
(defun calcupage (&optional (infixp t))
  (with-html-output-to-string
    (s nil :prologue t)
    (:html
      (:head
        (:title "Calculate things.")
        (:link :rel "stylesheet" :href "calcupage.css")
        (jsfile s "calcupage")
        (jsfile s "connects"))
      (:body
        (:div (:a :href "/input_matrix" "Create and edit")
              " vectors and matrices" :br
              "Use "
              (if infixp
                (htm (:a :href "/calcupage-prefix" "prefix"))
                (htm (:a :href "/calcupage-infix" "infix")))
              " notation instead." :br
              (:a :href "/" "Main") " Page")
        (:table
          :width "100%"
          (:tr
            (:th :rowspan "2" :class "linalg-buttons"
                 (htm-expand-known-if
                   s infixp (htm-linalg-buttons)))
            (:th :rowspan "2" :class "stdmath-buttons"
                 ;V Show operator buttons on left.V
                 (htm-expand-known-if
                   s infixp (htm-standard-math-buttons)))
            ;V Before variable names, show some easy-access buttons.V
            (:td :class "quick-buttons"
                 (htm-expand-known-if
                   s infixp (memory-buttons))
                 (princ
                   (load-time-value
                     (concatenate
                       'string "<br/>"
                       (clear-button)
                       (htm-input-button
                         "&quot;" "fn1" "\\\"") 
                       (ok-button)))
                   s))
            ;V Show variables on the top.V
            (:td
              (do ((lvpairs-left
                     `(("Numbers: " ,*saved-numbers*)
                       ("Vectors: " ,*saved-tuples*)
                       ("Matrices: " ,*saved-matrices*))))
                (nil)
                (princ (caar lvpairs-left) s)
                (dolist (k (cadar lvpairs-left))
                  (princ (simple-input-button k "varbutton") s))
                (unless (setq lvpairs-left (cdr lvpairs-left))
                  (return (values)))
                (princ "<br/>" s))))
          ;V main textarea V
          (:tr (:td  :colspan "2"
                     (:textarea :id "calcIn" :spellcheck "false"))))
        ;V Make a place to show results.V
        (:pre :id "resultPre")
        (if infixp (ps-tag s (setf process-loc "/process-infix"))
          (ps-tag s (setf process-loc "/process-prefix")))
        (ps-tag
          s (setf calc-in (.get-element-by-id document "calcIn")))))))

;V URL: /calcupage-prefix
(defun calcupage-prefix ()
  (calcupage nil))


