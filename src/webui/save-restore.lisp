
;V URL: capture_session
;V Return a file for the user to download V
;V containing all session variables.      V
(defun capture-session ()
  (setf (content-type) "application/octet-stream")
  (with-output-to-string (strm)
    (save-to-stream strm)))

;V URL: /save_restore
;V The main save and restore page which allows    V
;V the user to save and restore session variables.V
(defun save-restore-page (&optional (message ""))
  (with-html-output-to-string
    (strm nil :prologue t)
    (:html
      (:head
        (:title "Save/Restore Variables"))
      (:body
        (:div
          (:a :href "/input_matrix" "Create and edit")
          " vectors and matrices" :br
          (calcupage-link strm) :br
          (:a :href "/" "Main") " Page")
        :hr
        (:div (:a :href "/capture_session" "Get file")
              " containing current session's variables.")
        :hr
        (:div
          (:form :action "/restore_from_upload"
                 :enctype "multipart/form-data" :method "post"
                 "Add variables from session-restore file: " :br
                 (:input :type "file" :name "restorefile" :size "40") :br
                 (:input :type "submit" :value "Send")))
        :hr (:pre (princ message strm))))))

;V URL: /restore_from_upload
;V Add variables described in the uploaded file, V
;V overwriting any whose name already exists.    V
(defun restore-from-upload
  (&aux (file (car (post-parameter "restorefile"))))
  (with-open-file (strm file :direction :input)
    (save-restore-page
      (restore-from-stream strm))))

