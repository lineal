
(defmacro bind-reset ((&body args) . body)
  (let (tmp-lets s-sets)
    (loop :for a :in args
          :and b = (gentemp)
          :collect (list b a) :into l
          :collect a :into s
          :collect b :into s
          :finally
          (setq tmp-lets l
                s-sets (cons 'setq s)))
    `(let ,tmp-lets
       (unwind-protect
         ,(cons 'progn body)
         ,s-sets))))

(defun fresheval ()
  (bind-reset
    (*saved-numbers* *saved-tuples* *saved-matrices*)
    ;V Do file loading.V
    (compile-lineal :load-all nil)
    ;V Compile the javascript files.V
    (recompile-ps)))

;V Reload this file via the web.V
(defun reload (&aux (worked nil))
  (compile-if-new
    "reload" '(:relative "src" "webui")
    :fasl-dir '(:relative "fasl" "webui")
    :load-all nil)
  (unwind-protect
    (progn (fresheval)
           ;V say it worked V
           (setq worked t))
    ;V explicit return, needed if error thrown V
    (return-from
      reload
      (with-html-output-to-string
        (s nil :prologue t)
        (:html (:head (:title "Site reloaded."))
               (:body (:center
                        (princ (if worked "Everything worked"
                                 "eval errorz")
                               s))))))))

