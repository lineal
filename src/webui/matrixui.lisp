
;V URL: /matrix_select_contents
;V Return html code for the dropdown menu V
;V (html's "select" element) to list all  V
;V stored matrices.                       V
(defun matrix-select-contents ()
  (with-html-output-to-string
    (s nil)
    (:option "Name")
    (dolist (v *saved-tuples*)
      (with-html-output
        (s) (:option (princ v s))))
    (dolist (m *saved-matrices*)
      (with-html-output
        (s) (:option (princ m s))))))

;V URL: /post_matrix_input
;V Read input from client, store matrix.V
(defun get-matrix-input
  (&aux (cols (read-from-string (parameter "cols"))))
  (store-vrbl
    (parameter "name")
    ;V If there's only one column, user input a vector.V
    (if (= cols 1)
      (make-tuple
        :dim (read-from-string (parameter "rows"))
        :elems (mapcar #'car (read-from-string
                               (parameter "matrix"))))
      (make-mtrix
        :dimdom cols
        :dimcodom (read-from-string (parameter "rows"))
        :elems (read-from-string (parameter "matrix")))))
  (matrix-select-contents))

;V URL: /matrix_request
;V Pass javascript code back to the client for eval,  V
;V which stores the matrix's elements in a 2D array   V
;V named /matrix/. Also pass code that sets variables V
;V named /rows/ and /cols/ to their obvious values.   V
(defun matrix-request
  (&aux (name (parameter "name"))
        (s (make-string-output-stream)))
  "Return javascript code for storing"
  ;^ the named matrix in the variable: /matrix/ ^
  (let (matrows)
    (let ((m (symbol-value
               (find-symbol name :lineal.client-vars))))
      ;V State the rows and columns of the matrix, V
      ;V store the list of rows in /matrows/       V
      (typecase m
        (mtrix
          (format s "rows = ~D; cols = ~D;"
                  (mtrix-dimcodom m)
                  (mtrix-dimdom m))
          (setq matrows (mtrix-elems m)))
        (tuple
          (format s "rows = ~D; cols = ~D;"
                  (tuple-dim m) 1)
          (setq matrows (mapcar #'list (tuple-elems m))))
        (otherwise (throw 'unknown-variable name))))
    (princ "matrix = new Array(" s)
    (loop
      :for row :in matrows
      :and raystr = "new Array("
      :then ",new Array("
      :do (princ raystr s)
      :do (format s "\"~D\"" (car row))
      :do
      (loop
        :for elem :in (cdr row)
        :do (format s ",\"~D\"" elem))
      :do (princ ")" s))
    (princ ");" s)
    (get-output-stream-string s)))

;V URL /input_matrix
;V The matrix input page.V
(defun matrix-input-code
  (&aux (rows (parameter "rows"))
        (cols (parameter "cols")))
  (unless (integerp rows) (setq rows 2))
  (unless (integerp cols) (setq cols 2))
  (with-html-output-to-string
    (s nil :prologue t)
    (:html
      (:head
        (jsfile s "matrix-edit")
        (jsfile s "connects"))
      (:body
        (:div (:a :href "/" "Main") " Page" :br
              (calcupage-link s))
        :br
        (:div
          (:select
            :id "nameSelect"
            :onchange (ps-inline (select-name-change))
            (princ (matrix-select-contents) s))
          " " (:input :type "text" :onfocus "select()" :id "nameField")
          " " (:button :tabindex "-1"
                       :onclick (ps-inline (recall-matrix))
                       "Recall!"))
        :br
        (:form
          :action (ps-inline (reset-fields))
          (:table
            (:tr (:td "Rows") (:td "Columns") :td)
            (:tr (:td (:input :id "rowField" :onfocus "select()" :value rows))
                 (:td (:input :id "colField" :onfocus "select()" :value cols))
                 (:td (:button :tabindex "-1" "Resize!" )))))
        :br
        (:form
          :action (ps-inline (send-matrix))
          (:div :id "fieldDiv")
          (:div (:button "Update")))
        (:div :id "outDiv" "")
        (ps-tag
          s
          (setf name-field (.get-element-by-id document "nameField")
                name-select (.get-element-by-id document "nameSelect")
                row-field (.get-element-by-id document "rowField")
                col-field (.get-element-by-id document "colField")
                field-div (.get-element-by-id document "fieldDiv")
                out-div (.get-element-by-id document "outDiv"))
          (reset-fields))))))

