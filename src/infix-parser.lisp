
;;;; I suggest you move along to another file,
;;;; this one is terribly coded and in constant change.
;;;; But if you care to look around - good luck.

;;;; Heavily commented so I remember what happens here.
;
; General setup:
; An ordered stack of operations is build up as closures,
; each hold an accumulated value. When an operator of lesser
; "rank" (see rank constants below) is encountered, the stack unwinds
; recursively, passing the encountered rank and the accumulated
; value for the operation.
;
; Obviously there are different scopes where these operations apply,
; namely "paren" and "function" scopes. Both scopes have their own
; "read-eval" loop which terminate by the usual stack unwind and
; a thrown symbol corresponding to the scope:
; 'break-paren scope and 'break-fn-scope
;
; The order of events and the way the termination is handled
; is DIFFERENT between the two. Function scope is notably more
; complex because I don't require ONE function parameter to be
; enclosed in parentheses, multiple parameters require them.

#+(or) ; Uncomment to see results of parsing.
(trace eval-parsed)

;;; Define variables as constant integers
(defmacro enumerate-constants (&rest syms &aux (v 0))
  (declare (integer v))
  (cons 'progn
        (mapcar
          (lambda (k)
            (unless (symbolp k)
              (setf (values k v) (values-list k)))
            (prog1 `(defconstant ,k ,v)
              (incf v)))
          syms)))

;V Set precedence ranks.V
(enumerate-constants
  +base-rank+ 
  +paren-rank+ 
  +comma-rank+ 
  +fn-rank+ 
  +addn-rank+ 
  +subtrn-rank+ 
  +multn-rank+
  +divisn-rank+
  +factorial-rank+
  +exptn-rank+
  +vip-rank+);< Virtually infinite precedence.

;;; a b = a*b
(defun multn-if-last-read ()
  (declare (special *last-read*))
  (when *last-read*
    ;; Implicit multiplication, two
    ;; values were seperated by a space.
    (catch 'fn-scope
           (parsed-opern +multn-rank+ 'multn))))

;;; See if /parsed-thing/ can be interpreted
;;; in any way. If so, call success-fn, then
;;; finish interpreting.
(defun interpret-parsed
  (parsed-thing &optional (success-fn #'values))
  (declare (special *last-read*))
  (multn-if-last-read);< Check for implied multiplication.
  (if (symbolp parsed-thing)
    (when (boundp parsed-thing)
      (let ((val (symbol-value parsed-thing)))
        (funcall success-fn)
        (if (symbolp val)
          ;; A symbol representing a function
          ;; was read, change scope.
          (parse-function-scope val) ;(symbol-function val)
          (setq *last-read* parsed-thing))))
    (progn
      ;; The reader thinks it's something other
      ;; than a symbol, parser doesn't need to worry.
      (funcall success-fn)
      (setq *last-read* parsed-thing))))

;;; An invalid symbol was read,
;;; could be something like "34x"
(defun parse-compact (&optional sym &aux arrlen)
  (declare (special *parse-next* *last-read*))
  (if sym
    ;; Must create *parse-next* from a symbol
    (let ((sym-str (symbol-name sym)))
      (setq arrlen (length sym-str)
            *parse-next*
            (make-array arrlen
                        :element-type 'character
                        :fill-pointer (1- arrlen)
                        :displaced-to sym-str)))
    (setq arrlen (length *parse-next*)))
  (loop
    :for index :from (fill-pointer *parse-next*) :downto 1
    :do
    (let (successp)
      (setf (fill-pointer *parse-next*) index)
      (interpret-parsed
        (let ((*package* (find-package :lineal.client-vars)))
          (read-from-string *parse-next*))
        (lambda ()
          ;; We found valid input.
          (setf successp t)
          (if (= index arrlen)
            (setf *parse-next* nil)
            (setf
              (fill-pointer *parse-next*) arrlen
              *parse-next*
              (make-array (- arrlen index)
                          :element-type 'character
                          :fill-pointer t
                          :displaced-to *parse-next*
                          :displaced-index-offset index)))))
      (when successp (return)))
    :finally
    ;; Nothing matched; quit trying.
    (setf (fill-pointer *parse-next*) arrlen)
    (signal 'unbound-variable :name *parse-next*)))


;;; Everything in the file calls this
;;; read function on the infix stream.
(defun read-next-infix ()
  (declare (special *unwind-rank-fn* *last-read*
                    *parse-strm* *parse-next*))
  (if *parse-next* (parse-compact)
    (handler-case
      (let (this-read)
        (let ((*package* (find-package
                           :lineal.client-vars)))
          ;; Sometimes an exception is thrown and
          ;; control breaks to the current "read loop"
          (setq this-read (read *parse-strm*)))
        (when (and this-read
                   ;; Did not just read an operator.
                   (let (successp)
                     (interpret-parsed
                       this-read
                       (lambda () (setf successp t)))
                     (not successp)))
          ;; We have no idea wtf was just read.
          (parse-compact this-read)))
      (end-of-file
        (condit)
        (declare (ignore condit))
        ;; Don't check for unclosed parentheses,
        ;; be like the TI-83
        (funcall *unwind-rank-fn*
                 +base-rank+ *last-read*)))))

;;; This is a generic construct for a closure
;;; which will be stored as *unwind-rank-fn*
(defmacro opern-climber-lambda
  ((this-rank op-rank val) . body)
  `(lambda (,op-rank ,val)
     (unless ,val
       ;; A prefixed operator (see function below)
       ;; was parsed last, unary-pre-opern.
       (unary-pre-opern
         ,this-rank (the function ,op-rank)))
     (locally
       (declare (type (integer 0 ,+vip-rank+) ,op-rank))
       ,@body)))

;;; When something like 2 + -3 is encountered,
;;; the negative is what this function deals with.
(defun unary-pre-opern (prev-rank this-op-fn)
  (declare (special *unwind-rank-fn*))
  (let ((prev-unwind-rank-fn *unwind-rank-fn*)
        this-unwind-rank-fn)
    (setq
      this-unwind-rank-fn
      (opern-climber-lambda
        (prev-rank op-rank val)
        (if (and (< prev-rank op-rank)
                 (< +multn-rank+ op-rank))
          (progn (setq *unwind-rank-fn* 
                       this-unwind-rank-fn)
                 val)
          (funcall prev-unwind-rank-fn op-rank
                   (list this-op-fn val))))
      *unwind-rank-fn* this-unwind-rank-fn)
    (throw 'fn-scope (values))))

;;; A binary operator is parsed.
(defun parsed-opern (this-rank this-op-fn)
  (declare (special *unwind-rank-fn* *last-read*))
  (unless *last-read*
    (funcall *unwind-rank-fn* this-op-fn nil))
  (let* ((args (cons (funcall *unwind-rank-fn*
                              this-rank *last-read*)
                     nil))
         (tail args)
         (prev-unwind-rank-fn *unwind-rank-fn*)
         this-unwind-rank-fn)
    (setq
      *last-read* nil
      this-unwind-rank-fn
      (opern-climber-lambda
        (this-rank op-rank val)
        (cond
          ((= this-rank op-rank)
           (setq
             *unwind-rank-fn* this-unwind-rank-fn
             *last-read* nil
             tail (setf (cdr tail) (cons val nil)))
           (throw 'fn-scope (values)))
          ((< this-rank op-rank)
           (setq *unwind-rank-fn* this-unwind-rank-fn)
           val)
          (t
            (rplacd tail (cons val nil))
            (funcall prev-unwind-rank-fn op-rank
                     (cons this-op-fn args)))))
      *unwind-rank-fn* this-unwind-rank-fn)
    nil))

;;; Parse an exclaimation point character.
(defun factorial-reader (c strm)
  (declare (ignore c strm)
           (special *unwind-rank-fn* *last-read*))
  (unless *last-read*
    ;; Make sure any negatives are applied.
    (funcall *unwind-rank-fn*
             'lineal.overload::over-factorial nil))
  (setq *last-read*
        (list 'lineal.overload::factorial
              (funcall *unwind-rank-fn*
                       +factorial-rank+ *last-read*)))
  nil)

;;; Specialized function to gobble whitespace
;;; and return true if the terminating char
;;; is an opening parenthesis.
(defun open-paren-after-whitespace-peekp ()
  (declare (special *parse-strm* *parse-next*))
  (unless *parse-next*
    (do () ((not (char= #\Space (peek-char nil *parse-strm*)))
            (char=  #\( (peek-char nil *parse-strm*)))
      (read-char *parse-strm*))))

(defun closed-paren-peekp ()
  (declare (special *parse-strm* *parse-next*))
  (unless *parse-next*
    (char= #\) (peek-char nil *parse-strm*))))

;;; A function was read, passed as /this-fn/,
;;; its arguments have yet to be parsed.
(defun parse-function-scope (this-fn)
  (declare (special *unwind-rank-fn* *last-read* *parse-strm*))
  (when (open-paren-after-whitespace-peekp)
    ;; User chose to enclose the
    ;; argument(s) in parentheses.
    (read-char *parse-strm*)
    (paren-scope t)
    (setq *last-read*
          (if (consp *last-read*)
            ;V (apply this-fn *last-read*) V
            (cons this-fn *last-read*)
            ;V (funcall this-fn *last-read*) V
            (list this-fn *last-read*)))
    (return-from parse-function-scope (values)))
  (when (closed-paren-peekp)
    ;; Using notation like (f*g)(x)
    ;; (unimplemented)
    (setq *last-read* this-fn)
    (return-from parse-function-scope (values)))
  (let ((prev-unwind-rank-fn *unwind-rank-fn*)
        this-unwind-rank-fn)
    (setq
      *last-read* nil
      this-unwind-rank-fn
      (opern-climber-lambda
        (+fn-rank+ op-rank val)
        (cond
          ((< op-rank +fn-rank+)
           ;; Breaking from parens or program.
           (funcall prev-unwind-rank-fn
                    op-rank (if (consp val)
                              (cons this-fn val)
                              (list this-fn val))))
          ((= op-rank +fn-rank+)
           ;; Return from the function scope.
           (setq *unwind-rank-fn* prev-unwind-rank-fn
                 *last-read* (if (consp val)
                               (cons this-fn val)
                               (list this-fn val)))
           (throw 'break-fn-scope (values)))
          (t ;V Stay in the function scope.V
            (setq *unwind-rank-fn* this-unwind-rank-fn)
            val)))
      *unwind-rank-fn* this-unwind-rank-fn)
    (catch
      'break-fn-scope
      (do () (nil)
        (catch 'fn-scope
               (read-next-infix))))
    (funcall *unwind-rank-fn*
             +fn-rank+ *last-read*)))

;;; "read-eval" loop used by process-infix-from-stream
;;; and open-paren-reader
(defun parse-infix ()
  (declare (special *unwind-rank-fn* *last-read*))
  (let ((prev-unwind-rank-fn *unwind-rank-fn*)
        this-unwind-rank-fn)
    (setq
      *last-read* nil
      this-unwind-rank-fn
      (opern-climber-lambda
        (+paren-rank+ op-rank val)
        (cond
          ((< op-rank +paren-rank+)
           (funcall prev-unwind-rank-fn
                    op-rank val))
          ((= op-rank +paren-rank+)
           ;; Closing paren encountered.
           (setq *unwind-rank-fn* prev-unwind-rank-fn
                 *last-read* val)
           (throw 'break-paren-scope (values)))
          (t
            (setq *unwind-rank-fn* this-unwind-rank-fn)
            val)))
      *unwind-rank-fn* this-unwind-rank-fn)
    (do () (nil)
      (catch
        'break-fn-scope
        (catch
          'fn-scope
          (read-next-infix))))))

(defun paren-scope (paramsp)
  (declare (special *last-read*))
  (multn-if-last-read)
  (catch 'break-paren-scope (parse-infix))
  (when (and (not paramsp)
             (consp *last-read*)
             (not (and (symbolp (car *last-read*))
                       (fboundp (car *last-read*)))))
    ;; Convert the list into a tuple
    ;; since it's not a function's parameters.
    (setq *last-read*
          (cons 'lineal.overload::over-vcat *last-read*))))

;;; A parenthesis has been opened!
(defun open-paren-reader (strm ch)
  (declare (ignore strm ch))
  (paren-scope nil)
  nil)

;;; Unwind the operation stack.
;;; When the paren closure is reached,
;;; *last-read* is set to the value calculated within
;;; the parenthesis and 'break-paren-scope is thrown.
(defun close-paren-reader (strm ch)
  (declare (ignore strm ch)
           (special *unwind-rank-fn* *last-read*))
  (funcall *unwind-rank-fn*
           +paren-rank+ *last-read*))

;;; Much like open-paren-reader
;;; but creates a row matrix.
;;; (uses close-paren-reader
;;;  for closed brackets)
(defun open-bracket-reader (strm ch)
  (declare (ignore strm ch)
           (special *last-read*))
  (multn-if-last-read)
  (catch 'break-paren-scope
         (parse-infix))
  (when (consp *last-read*)
    (setq *last-read*
          (cons 'lineal.overload::over-cat *last-read*)))
  nil)

;;; If *last-read* is nil, an operator
;;; was read last, we can't logically
;;; break from a function in that case.
(defun space-reader (strm ch)
  (declare (ignore strm ch)
           (special *last-read*))
  (when *last-read*
    (throw 'break-fn-scope (values))))

(defun comma-reader (strm ch)
  (declare (ignore strm ch)
           (special *unwind-rank-fn* *last-read*))
  (let* ((lis (cons (funcall *unwind-rank-fn*
                             +comma-rank+ *last-read*)
                    nil))
         (tail lis)
         (prev-unwind-rank-fn *unwind-rank-fn*)
         this-unwind-rank-fn)
    (setq
      *last-read* nil
      this-unwind-rank-fn
      (opern-climber-lambda
        (+comma-rank+ op-rank val)
        (cond
          ((= +comma-rank+ op-rank)
           (setq
             *unwind-rank-fn*
             this-unwind-rank-fn
             *last-read* nil
             tail
             (cdr (rplacd tail (cons val nil))))
           (throw 'break-fn-scope (values)))
          ((< +comma-rank+ op-rank)
           ;; This is now the base rank,
           (setq *unwind-rank-fn*
                 this-unwind-rank-fn)
           val)
          (t (rplacd tail (cons val nil)) 
             (funcall prev-unwind-rank-fn op-rank
                      lis))))
      *unwind-rank-fn* this-unwind-rank-fn)
    nil))

(defun set-opern-reader (ch this-rank this-op-fn)
  (set-macro-character
    ch
    (lambda (strm ch)
      (declare (ignore strm ch))
      (parsed-opern this-rank this-op-fn))))

(defparameter *infix-readtable* (copy-readtable))

(let ((*readtable* *infix-readtable*))
    (setf (readtable-case *readtable*) :preserve)
    (set-macro-character #\( #'open-paren-reader)
    (set-macro-character #\) #'close-paren-reader)
    (set-macro-character #\[ #'open-bracket-reader)
    (set-macro-character #\] #'close-paren-reader)
    (set-macro-character #\Space #'space-reader)
    (set-macro-character #\, #'comma-reader)
    (set-opern-reader #\+ +addn-rank+ 'addn)
    (set-opern-reader #\- +subtrn-rank+ 'subtrn)
    (set-opern-reader #\* +multn-rank+ 'multn)
    (set-opern-reader #\/ +divisn-rank+ 'divisn)
    (set-macro-character #\! #'factorial-reader)
    (set-opern-reader #\^ +exptn-rank+ 'exptn))

(defun parse-infix-from-stream (strm)
  (let ((*readtable* *infix-readtable*)
        (*parse-strm* strm);< Stream to parse.
        *parse-next*;< Temporary buffer if we overparsed.
        (*unwind-rank-fn*
          (lambda (op-rank val)
            (declare (ignore op-rank))
            (throw 'end-result val)))
        *last-read*)
    (declare (special
               *parse-strm* *parse-next*
               *unwind-rank-fn* *last-read*))
    (handler-case
      (catch 'over-ex
             (catch 'end-result
                    (parse-infix)))
      (control-error
        (condit)
        (declare (ignore condit))
        (format
          nil "Likely too many parens! ~
          (as if there were such a thing)~%"))
      (unbound-variable
        (condit)
        (format nil "I don't understand: ~A~%"
                (cell-error-name condit)))
      (error
        (condit)
        (format
          nil "Evaluation flopped, perhaps bad input?~%~
          Debug info: ~A~%" condit)))))

(defun process-infix-from-stream (strm)
  (eval-parsed (parse-infix-from-stream strm)))

(defun process-input-from-stream
  (strm &optional (infixp t))
  (let ((*read-default-float-format* 'double-float)
        (*read-eval* nil))
    (if infixp (process-infix-from-stream strm)
      (process-prefix-from-stream strm))))

(defun process-input-from-string
  (text &optional (infixp t))
  (with-input-from-string (strm text)
    (process-input-from-stream strm infixp)))

