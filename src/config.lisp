
; To uncomment an option, change #+(or) to #-(or)
; All pathnames are relative to this file's directory.
;
; Lineal will output confirmation of all options
; set here near the end of it's load-time output.
; 
; If any of these options is true/false, the actual
; value of the option is not checked, only if it's bound.
; ie: if the option is explicitly set to nil,
;    (set 'option nil)
; it is still treated as true.
; Use (makunbound 'option) to falsify.


; On boot, restore the session specified in the file.
#+(or) (set 'restore-from-file
            (make-pathname :name "captured_session"))
; To use this effectively, call
; (local-capture)
; before quitting to save the file 

