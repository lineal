
(defun over-det (a)
  (if (= (mtrix-dimdom a) (mtrix-dimcodom a))
    (det (mtrix-elems a))
    (throw 'over-ex "Determinants only work with square matrices.")))

(defun over-r-ef (&rest args)
  (let ((a (apply #'over-cat args)))
    (make-mtrix-like
      a (r-ef (mtrix-elems a)))))

(defun over-rr-ef (&rest args)
  (let ((a (apply #'over-cat args)))
    (make-mtrix-like
      a (rr-ef (mtrix-elems a)))))

(defmethod over-multv-inverse ((a mtrix))
  (when (zerop (over-det a))
    (throw 'over-ex "Determinant is zero, no inverse exists."))
  (make-mtrix-like
    a (mtrix-inverse (mtrix-elems a))))

;V Raise matrix /a/ to exponent /b/.V
(defmethod expt2n ((a mtrix) (b integer))
  ;V Matrix must be square.V
  (unless (= (mtrix-dimdom a)
             (mtrix-dimcodom a))
    (throw 'over-ex "Only square matrices can be raised to exponents."))
  ;V If negative power, raise inverse V
  ;V of /a/ to negative /b/ power.    V
  (when (minusp b)
    (setq a (over-multv-inverse a)
          b (- b)))
  ;V When /b/ is zero, return the identity matrix.V
  (if (zerop b)  
    (make-mtrix
      :dimdom (mtrix-dimdom a)
      :dimcodom (mtrix-dimdom a)
      :elems (mtrix-square-identity (mtrix-dimdom a)))
    (loop :repeat b
          :for c = a :then (mult2n a c)
          :finally (return c))))

