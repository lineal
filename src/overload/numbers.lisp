
(defmethod over-multv-inverse ((a number)) (/ a))
(defmethod expt2n (a b) (expt a b))

(defmethod add2n ((a number) (b number)) (+ a b))
(defmethod subtr2n ((a number) (b number)) (- a b))
(defmethod mult2n ((a number) (b number)) (* a b))
(defmethod divis2n ((a number) (b number)) (/ a b))

(defun over-sqrt (n)
  (if (= n -1)
    (complex 0 1)
    (sqrt n)))

(defun over-factorial (n)
  (declare (type (integer 0 *) n))
  (factorial n))

