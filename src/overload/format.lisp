
(defgeneric over-format (a strm))

(defmethod over-format ((n float) s)
  (when (minusp n)
    (write-char #\- s)
    (setq n (- n)))
  ;; Cut off after 7 post-decimal digits
  (do ((igr (round (* n 10000000)))
       digit
       (lis nil)
       (itersrem 7 (1- itersrem)))
    ((zerop itersrem)
     (format s "~D.~{~D~}" igr lis))
    (multiple-value-setq
      (igr digit) (floor igr 10))
    (when (or lis (not (zerop digit)))
      (push digit lis))))

(defmethod over-format ((n complex) s)
  (cond
    ((zerop (imagpart n))
     (over-format (realpart n) s))
    ((zerop (realpart n))
     (case (imagpart n) (1)
       (-1 (write-char #\- s))
       (t (over-format (imagpart n) s)))
     (write-char #\i s))
    (t (over-format (realpart n) s)
       (let ((tmp
               (if (plusp (imagpart n))
                 (progn (princ " + " s)
                        (imagpart n))
                 (progn (princ " - " s)
                        (- (imagpart n))))))
         (unless (= tmp 1)
           (over-format tmp s)))
       (write-char #\i s))))

(defmethod over-format ((a number) s)
  (princ a s))
(defmethod over-format ((a tuple) s)
  (output-tuple (tuple-elems a) s))
(defmethod over-format ((a mtrix) s)
  (output-matrix (mtrix-elems a) s))

(defmethod over-format ((a string) strm)
  (princ a strm)
  (fresh-line strm))

