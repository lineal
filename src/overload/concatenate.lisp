
(defun over-cat (&rest args)
  (if args
    (cat-list args)
    (throw 'over-ex "Can't cat nothing.")))

(defun over-vcat (&rest args)
  (if args
    (vcat-list args)
    (throw 'over-ex "Can't vcat nothing.")))

(defmacro cat2-fill
  (((arow s-arows)
    (brow s-brows)
    rows)
   s-cat
   (width
     (s-awidth s-azeros)
     (s-bwidth s-bzeros))
   s-return)
  (let ((row (gensym))
        (tail (gensym))
        (arem (gensym)) (brem (gensym))
        (finished (gensym)))
    `(tails-do*
       (((,row ,rows ,tail))
        (,arem ,s-arows (cdr ,arem))
        (,brem ,s-brows (cdr ,brem))
        (,finished nil (not (and ,arem ,brem))))
       (,finished
         (rplacd
           ,tail
           (if ,arem
             (let* (,@(when s-bwidth `((,width ,s-bwidth)))
                     (,brow ,s-bzeros))
               (mapcar #'(lambda (,arow) ,s-cat) ,arem))
             (when ,brem
               (let* (,@(when s-awidth `((,width ,s-awidth)))
                       (,arow ,s-azeros))
                 (mapcar #'(lambda (,brow) ,s-cat) ,brem)))))
         ,s-return)
       (let ((,arow (car ,arem))
             (,brow (car ,brem)))
         (setq ,row ,s-cat)))))


(defmethod cat2 ((a number) (b number))
  (make-mtrix :dimdom 2 :dimcodom 1
              :elems (list (list a b))))

(defmethod vcat2 ((a number) (b number))
  (make-tuple :dim 2 :elems (list a b)))

(defmethod cat2 ((k number) (u tuple))
  (make-mtrix :dimdom 2 :dimcodom (tuple-dim u)
              :elems (mapcar #'(lambda (x) (list k x))
                             (tuple-elems u))))

(defmethod cat2 ((u tuple) (k number))
  (make-mtrix :dimdom 2 :dimcodom (tuple-dim u)
              :elems (mapcar #'(lambda (x) (list x k))
                             (tuple-elems u))))

(defmethod vcat2 ((k number) (u tuple))
  (make-tuple :dim (1+ (tuple-dim u))
              :elems (cons k (tuple-elems u))))

(defmethod vcat2 ((u tuple) (k number))
  (make-tuple :dim (1+ (tuple-dim u))
              :elems `(,@(tuple-elems u) ,k)))

;V Align two tuples into a column matrix.V
(defmethod cat2 ((u tuple) (v tuple))
  (cat2-fill
    ((uelem (tuple-elems u))
     (velem (tuple-elems v))
     rows)
    (list uelem velem)
    (nil (nil 0) (nil 0))
    (make-mtrix :dimdom 2
                :dimcodom (max (tuple-dim u)
                               (tuple-dim v))
                :elems rows)))

;V Merge two tuples together into one long vector.V
(defmethod vcat2 ((u tuple) (v tuple))
  (make-tuple :dim (+ (tuple-dim u) (tuple-dim v))
              :elems (append (tuple-elems u)
                             (tuple-elems v))))

;V Add a column vector /u/ to  V
;V the left side of matrix /a/.V
(defmethod cat2 ((u tuple) (a mtrix))
  (cat2-fill
    ((uelem (tuple-elems u))
     (arow (mtrix-elems a))
     rows)
    (cons uelem arow)
    (width
      (nil 0)
      ((mtrix-dimdom a)
       (make-list width :initial-element 0)))
    (make-mtrix :dimdom (1+ (mtrix-dimdom a))
                :dimcodom (max (tuple-dim u)
                               (mtrix-dimcodom a))
                :elems rows)))

(defmethod cat2 ((a mtrix) (u tuple))
  (cat2-fill
    ((arow (mtrix-elems a))
     (uelem (tuple-elems u))
     rows)
    `(,@arow ,uelem)
    (width
      ((mtrix-dimdom a)
       (make-list width :initial-element 0))
      (nil 0))
    (make-mtrix :dimdom (1+ (mtrix-dimdom a))
                :dimcodom (mtrix-dimcodom a)
                :elems rows)))

(defmethod cat2 ((a mtrix) (b mtrix))
  (cat2-fill
    ((arow (mtrix-elems a))
     (brow (mtrix-elems b))
     rows)
    (append arow brow)
    (width
      ((mtrix-dimdom a)
       (make-list width :initial-element 0))
      ((mtrix-dimdom b)
       (make-list width :initial-element 0)))
    (make-mtrix :dimdom (+ (mtrix-dimdom a)
                           (mtrix-dimdom b))
                :dimcodom (max (mtrix-dimcodom a)
                               (mtrix-dimcodom b))
                :elems rows)))

(defmethod vcat2 ((u tuple) (a mtrix))
  (let ((uwid (tuple-dim u))
        (awid (mtrix-dimdom a))
        (rows (cons (tuple-elems u)
                    (mtrix-elems a))))
    (if (< uwid awid)
      (rplaca rows
              (append
                (car rows)
                (make-list (- awid uwid)
                           :initial-element 0)))
      (unless (= uwid awid)
        (shiftf awid uwid (- uwid awid))
        (rplacd
          rows
          (mapcar
            #'(lambda (row)
                (append
                  row
                  (make-list uwid :initial-element 0)))
            (cdr rows)))))
    (make-mtrix :dimdom awid
                :dimcodom (1+ (mtrix-dimcodom a))
                :elems rows)))

(defmethod vcat2 ((a mtrix) (u tuple))
  (let ((awid (mtrix-dimdom a))
        (uwid (tuple-dim u)))
    (make-mtrix
      :dimcodom (1+ (mtrix-dimcodom a))
      :elems
      (if (< uwid awid)
        `(,@(mtrix-elems a)
           ,(append (tuple-elems u)
                    (make-list (- awid uwid)
                               :initial-element 0)))
        (if (= uwid awid)
          `(,@(mtrix-elems a) ,(tuple-elems u))
          (progn
            (shiftf awid uwid (- uwid awid))
            (tails-do*
              (((row rows tail))
               (arem (mtrix-elems a) (cdr arem)))
              ((not arem)
               (rplacd tail (cons (tuple-elems u) nil))
               rows)
              (setq row
                    (append
                      (car arem)
                      (make-list
                        uwid :initial-element 0)))))))
      :dimdom awid)))

;V Join a column of /k/'s onto the left of /a/.V
(defmethod cat2 ((k number) (a mtrix))
  (make-mtrix
    :dimdom (1+ (mtrix-dimdom a))
    :dimcodom (mtrix-dimcodom a)
    :elems (mapcar #'(lambda (row) (cons k row))
                   (mtrix-elems a))))

;V Join a column of /k/'s onto the right of /a/.V
(defmethod cat2 ((a mtrix) (k number))
  (make-mtrix
    :dimdom (1+ (mtrix-dimdom a))
    :dimcodom (mtrix-dimcodom a)
    :elems (mapcar #'(lambda (row) `(,@row ,k))
                   (mtrix-elems a))))

;V Join a row of /k/'s onto the top of /a/.V
(defmethod vcat2 ((k number) (a mtrix))
  (make-mtrix
    :dimdom (mtrix-dimdom a)
    :dimcodom (1+ (mtrix-dimcodom a))
    :elems (cons (make-list (mtrix-dimdom a)
                            :initial-element k)
                 (mtrix-elems a))))

;V Join a row of /k/'s onto the bottom of /a/.V
(defmethod vcat2 ((a mtrix) (k number))
  (make-mtrix
    :dimdom (mtrix-dimdom a)
    :dimcodom (1+ (mtrix-dimcodom a))
    :elems `(,@(mtrix-elems a)
              ,(make-list (mtrix-dimdom a)
                          :initial-element k))))

(defmethod vcat2 ((a mtrix) (b mtrix))
  (let* ((awid (mtrix-dimdom a))
         (bwid (mtrix-dimdom b))
         (finwid (max awid bwid))
         (zeros (make-list (abs (- awid bwid))
                           :initial-element 0)))
    (make-mtrix
      :dimdom finwid
      :dimcodom (+ (mtrix-dimcodom a)
                   (mtrix-dimcodom b))
      :elems (append
               (if (= awid finwid) (mtrix-elems a)
                 (mapcar #'(lambda (row)
                             (append row zeros))
                         (mtrix-elems a)))
               (if (= bwid finwid) (mtrix-elems b)
                 (mapcar #'(lambda (row)
                             (append row zeros))
                         (mtrix-elems b)))))))


