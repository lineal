
(defstruct tuple
  (dim 0 :type (integer 0 *))
  (elems nil :type list))

(defmacro tuple-list (u &optional len)
  `(make-tuple :dim ,(if len len `(length ,u))
               :elems ,u))

;;; Make sure tuple /u/ is represented by a cons.
;;; Optionally do dimension checking.
(defmacro ensure-tuple-is-cons
  (u &optional dim str)
  (if dim
    `(if (consp ,u)
       (unless (= (length ,u) ,dim)
         (throw 'over-ex ,str))
       (if (= (tuple-dim ,u) ,dim)
         (setq ,u (tuple-elems ,u))
         (throw 'over-ex ,str)))
    `(unless (consp ,u)
       (setq ,u (tuple-elems ,u)))))

(defun over-dot-prod (u v)
  (declare (type (or tuple cons) u)
           (type (or tuple cons) v))
  (dot-prod
    (if (consp u) u (tuple-elems u))
    (if (consp v) v (tuple-elems v))))

(defun over-cross-prod (u v)
  (declare (type (or tuple cons) u)
           (type (or tuple cons) v))
  (ensure-tuple-is-cons
    u 3 "cross products require 3-D vectors")
  (ensure-tuple-is-cons
    v 3 "cross products require 3-D vectors")
  (make-tuple :dim 3 :elems (tuple-cross3 u v)))

;;; Projection of /u/ onto /v/.
(defun over-proj (u v)
  (declare (type (or tuple cons) u)
           (type (or tuple cons) v))
  (let ((tdim 
          (if (consp u) (length u)
            (prog1 (tuple-dim u)
              (setq u (tuple-elems u))))))
    (ensure-tuple-is-cons
      v tdim "proj requires vectors of same dimension")
    (make-tuple :dim tdim
                :elems (tuple-proj u v))))

;;; Orthogonal component of the
;;; projection of /u/ onto /v/.
(defun over-orth (u v)
  (declare (type (or tuple cons) u)
           (type (or tuple cons) v))
  (let ((tdim 
          (if (consp u) (length u)
            (prog1 (tuple-dim u)
              (setq u (tuple-elems u))))))
    (ensure-tuple-is-cons
      v tdim "orth requires vectors of same dimension")
    (make-tuple :dim tdim
                :elems (tuple-orth u v))))

;;; u + v
(defmethod add2n ((u tuple) (v tuple))
  (if (= (tuple-dim u) (tuple-dim v))
    (make-tuple
      :dim (tuple-dim u)
      :elems (tuple-addn (tuple-elems u) (tuple-elems v)))
    (throw 'over-ex "don't add vectors of different dimension")))

;;; u - v
(defmethod subtr2n ((u tuple) (v tuple))
  (if (= (tuple-dim u) (tuple-dim v))
    (make-tuple
      :dim (tuple-dim u)
      :elems (tuple-subtrn (tuple-elems u) (tuple-elems v)))
    (throw 'over-ex "don't subtract vectors of different dimension")))


;;; k * u
(defmethod mult2n ((k number) (u tuple))
  (make-tuple :dim (tuple-dim u)
              :elems (scalar-tuple-multn
                       k (tuple-elems u))))


(defmethod mult2n ((u tuple) (k number))
  (make-tuple :dim (tuple-dim u)
              :elems (scalar-tuple-multn
                       k (tuple-elems u))))


;;; u / k
(defmethod divis2n ((u tuple) (k number))
  (make-tuple :dim (tuple-dim u)
              :elems (tuple-scalar-divisn
                       (tuple-elems u) k)))


