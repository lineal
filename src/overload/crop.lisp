
(defmethod over-crop ((n integer) (u tuple))
  (declare (ignore n)) u)
(defmethod over-crop ((u tuple) (n integer))
  (declare (ignore n)) u)

(defmethod over-vcrop ((n integer) (u tuple))
  (if (= 1 n)
    (car (tuple-elems u))
    (loop :with dim = (min n (tuple-dim u))
          :for elem :in (tuple-elems u)
          :repeat dim
          :collect elem :into elems
          :finally
          (return (make-tuple :dim dim :elems elems)))))


(defmethod over-vcrop ((u tuple) (n integer))
  (if (= 1 n)
    (car (last (tuple-elems u)))
    (let ((dim (min n (tuple-dim u))))
      (make-tuple :dim dim
                  :elems (nthcdr (- (tuple-dim u) dim)
                                 (tuple-elems u))))))


(defmethod over-crop ((n integer) (a mtrix))
  (if (= n 1)
    (make-tuple
      :dim (mtrix-dimcodom a)
      :elems (mapcar #'car (mtrix-elems a)))
    (loop 
      :with width = (min n (mtrix-dimdom a))
      :for row :in (mtrix-elems a)
      :collect (loop :for elem :in row
                     :repeat width
                     :collect elem)
      :into rows
      :finally
      (return (make-mtrix
                :dimdom width
                :dimcodom (mtrix-dimcodom a)
                :elems rows)))))

(defmethod over-crop ((a mtrix) (n integer))
  (if (= n 1)
    (make-tuple
      :dim (mtrix-dimcodom a)
      :elems (mapcar (lambda (row) (car (last row)))
                     (mtrix-elems a)))
    (loop
      :with width = (min n (mtrix-dimdom a))
      :with prune-count = (- (mtrix-dimdom a) width)
      :for row :in (mtrix-elems a)
      :collect (nthcdr prune-count row) :into rows
      :finally
      (return (make-mtrix
                :dimdom width
                :dimcodom (mtrix-dimcodom a)
                :elems rows)))))

(defmethod over-vcrop ((n integer) (a mtrix))
  (let ((height (min n (mtrix-dimcodom a))))
    (make-mtrix
      :dimdom (mtrix-dimdom a)
      :dimcodom height
      :elems (loop :for row :in (mtrix-elems a)
                   :repeat height
                   :collect row))))

(defmethod over-vcrop ((a mtrix) (n integer))
  (let ((height (min n (mtrix-dimcodom a))))
    (make-mtrix
      :dimdom (mtrix-dimdom a)
      :dimcodom height
      :elems (nthcdr (- (mtrix-dimcodom a) height)
                     (mtrix-elems a)))))

(defmethod over-crop :around ((a integer) b)
  (if (plusp a) (call-next-method a b)
    (throw 'over-ex "Can only crop a positive number of columns.")))
(defmethod over-crop :around (a (b integer))
  (if (plusp b) (call-next-method a b)
    (throw 'over-ex "Can only crop a positive number of columns.")))

(defmethod over-vcrop :around ((a integer) b)
  (if (plusp a) (call-next-method a b)
    (throw 'over-ex "Can only vcrop a positive number of rows.")))
(defmethod over-vcrop :around (a (b integer))
  (if (plusp b) (call-next-method a b)
    (throw 'over-ex "Can only vcrop a positive number of rows.")))

(defmethod over-crop (a b)
  (throw 'over-ex
         (format nil "crop usage: (crop [n] [thing]) ~
                 or (crop [thing] [n])~%where [n] is ~
                 the number of columns you want.")))

(defmethod over-vcrop (a b)
  (throw 'over-ex
         (format nil "vcrop usage: (vcrop [n] [thing]) ~
                 or (vcrop [thing] [n])~%where [n] is ~
                 the number of rows you want.")))

