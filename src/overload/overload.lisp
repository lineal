
(defpackage :lineal.overload
  (:use :cl)
  (:export *accep-table* mtrix tuple
           make-tuple tuple-dim tuple-elems
           make-mtrix mtrix-dimdom mtrix-dimcodom mtrix-elems
           over-format over-ex
           addn subtrn multn divisn exptn)
  (:import-from
    :lineal.math
    det
    dot-prod
    factorial
    mtrix-addn
    mtrix-augment
    mtrix-coltuple-multn
    mtrix-inverse
    mtrix-multn
    mtrix-mult2n
    mtrix-square-identity
    mtrix-subtrn
    mtrix-transpose
    nAr nCr nPr
    nullspace
    output-tuple
    output-matrix
    r-ef
    rr-ef
    scalar-mtrix-multn
    scalar-tuple-multn
    tuple-addn
    tuple-cross3
    tuple-magnitude
    tuple-orth
    tuple-proj
    tuple-scalar-divisn
    tuple-subtrn))

(in-package :lineal.overload)

;;; Multiplicative Inverse
(defgeneric over-multv-inverse (a))
;;; Concatenate Horizontally
(defgeneric cat2 (term1 term2))
(defun cat-list (elems)
  (if (cdr elems)
    (reduce #'cat2 elems :from-end t)
    (car elems)))
;;; Concatenate Vertically (or vector-wise)
(defgeneric vcat2 (a b))
(defun vcat-list (elems)
  (if (cdr elems)
    (reduce #'vcat2 elems :from-end t)
    (car elems)))

(defgeneric over-crop (a b))
(defgeneric over-vcrop (a b))

(defgeneric over-transpose (a))
(defmethod over-transpose (a)
  (throw 'over-ex
         "You can only tranpose vectors and matrices."))


;;; Standard operations.
(defgeneric add2n (a b))
(defgeneric subtr2n (a b))
(defgeneric mult2n (a b))
(defgeneric divis2n (a b))
(defgeneric expt2n (a b))

(defun addn (&rest elems)
  (reduce #'add2n elems))

(defun subtrn (&rest elems)
  (if (cdr elems)
    (reduce #'subtr2n elems)
    ;V If only one element, follow V
    ;V lisp style by negating it.  V 
    (mult2n -1 (car elems))))

(defun multn (&rest elems)
  (reduce #'mult2n elems))

(defun divisn (&rest elems)
  (if (cdr elems)
    (reduce #'divis2n elems)
    (over-multv-inverse (car elems))))

(defun exptn (&rest elems)
  (reduce #'expt2n elems))


