
(defstruct mtrix
  (dimdom 0 :type integer)
  (dimcodom 0 :type integer)
  (elems nil :type list))

(defun over-trace (a)
  (unless (= (mtrix-dimdom a) (mtrix-dimcodom a))
    (throw 'over-ex
           "Can only get the trace of a square matrix."))
  (loop :for i :from 0
        :for row :in (mtrix-elems a)
        :sum (elt row i)))

(defun make-mtrix-like (a elems)
  (make-mtrix
    :dimdom (mtrix-dimdom a)
    :dimcodom (mtrix-dimcodom a)
    :elems elems))

(defmethod over-transpose ((u tuple))
  (make-mtrix
    :dimdom (tuple-dim u)
    :dimcodom 1
    :elems (list (tuple-elems u))))

; Return the transpose of a given matrix.V
(defmethod over-transpose ((a mtrix))
  (make-mtrix
    :dimdom (mtrix-dimcodom a)
    :dimcodom (mtrix-dimdom a)
    :elems (mtrix-transpose (mtrix-elems a))))

(defmethod add2n ((a mtrix) (b mtrix))
  (if (and (= (mtrix-dimdom a) (mtrix-dimdom b))
           (= (mtrix-dimcodom a) (mtrix-dimcodom b)))
    (make-mtrix-like
      a (mtrix-addn (mtrix-elems a)
                    (mtrix-elems b)))))

(defmethod subtr2n ((a mtrix) (b mtrix))
  (if (and (= (mtrix-dimdom a) (mtrix-dimdom b))
           (= (mtrix-dimcodom a) (mtrix-dimcodom b)))
    (make-mtrix-like
      a (mtrix-subtrn (mtrix-elems a)
                      (mtrix-elems b)))))

(defmethod mult2n ((a number) (b mtrix))
  (make-mtrix-like
    b (scalar-mtrix-multn a (mtrix-elems b))))

(defmethod mult2n ((a mtrix) (b number))
  (make-mtrix-like
    a (scalar-mtrix-multn b (mtrix-elems a))))

(defmethod mult2n ((a mtrix) (b tuple))
  (if (= (mtrix-dimdom a) (tuple-dim b))
    (make-tuple
      :dim (mtrix-dimcodom a) :elems
      (mtrix-coltuple-multn
        (mtrix-elems a) (tuple-elems b)))
    (throw 'over-ex "Bad dimensions for matrix-vector multiplication.")))

(defmethod mult2n ((a tuple) (b mtrix))
  (if (= (tuple-dim a) (mtrix-dimcodom b))
    (make-tuple
      :dim (mtrix-dimdom b) :elems
      (car (mtrix-mult2n
             (list (tuple-elems a)) (mtrix-elems b))))
    (throw 'over-ex "Bad dimensions for vector-matrix multiplication.")))

(defmethod mult2n ((a mtrix) (b mtrix))
  (if (= (mtrix-dimdom a) (mtrix-dimcodom b))
    (make-mtrix
      :dimdom (mtrix-dimdom b)
      :dimcodom (mtrix-dimcodom a) :elems
      (mtrix-mult2n (mtrix-elems a) (mtrix-elems b)))
    (throw 'over-ex "Mismatched dimensions for matrix multiplication.")))

(defmethod divis2n ((a mtrix) (b number))
  (make-mtrix-like
    a (scalar-mtrix-multn
        (/ b) (mtrix-elems a))))

