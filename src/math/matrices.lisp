
(defun mtrix-transpose (mtrix)
  (apply #'mapcar #'list mtrix))

(defun mtrix-addn (&rest matrs)
  (apply #'mapcar #'tuple-addn matrs)) 

(defun mtrix-subtrn (&rest matrs)
  (apply #'mapcar #'tuple-subtrn matrs))

(defun scalar-mtrix-multn (k a)
  "Multiply a scalar /k/ and matrix /a/."
  (mapcar
    (lambda (arow)
      (scalar-tuple-multn k arow))
    a))

(defun mtrix-mult2n (a b)
  "Multiply 2 matrices."
  (mapcar
    (lambda (arow)
      (apply #'tuple-addn 
             (mapcar #'scalar-tuple-multn
                     arow b)))
    a))

(defun mtrix-multn (&rest mats)
  "Standard matrix multiplication."
  (do ((a (car mats) (mtrix-mult2n a (car tomult)))
       (tomult (cdr mats) (cdr tomult)))
    ((endp tomult) a)))

(defun mtrix-coltuple-multn (a u)
  (declare (inline dot-prod))
  "Multiply matrix /a/ and column vector /u/."
  (loop :for row :in a
        :collect (dot-prod row u)))


