
; u - kv
(defun tuple-multiple-nsubtrn (u k v)
  "Vector /u/ minus scalar /k/ times vector /v/"
  (do ((u-rem u (cdr u-rem))
       (v-rem v (cdr v-rem)))
    ((not (or u-rem v-rem)) u)
    (decf (car u-rem) (* k (car v-rem)))))

(defun row-clear-leading-with (u rows)
  (declare (inline tuple-multiple-nsubtrn))
  "Clear the first column with the leading-1 row."
  (loop :for to-clear :in rows :collect
        (cdr (tuple-multiple-nsubtrn
               to-clear (car to-clear) u))))

(defun row-reduction (a incheck-fn nolead-fn)
  (when (and a (car a))
    (if (zerop (caar a))
      (loop
        :with toprow = (car a)
        :for rows :on a
        :unless (zerop (caar rows)) :do
        (progn
          (rotatef (caar rows) (car toprow))
          (rotatef (cdar rows) (cdr toprow))
          (funcall incheck-fn (cdr nat-zero)
                   toprow rows)
          (return))
        :collect (cdar rows) :into nat-zero
        :finally (funcall nolead-fn nat-zero))
      (funcall incheck-fn nil (car a) (cdr a)))))

(defun det (a &optional (destroy nil)
              &aux (curr-det 1))
  "Determinant"
  (labels
    ((fix-in-lead
       (row toclear)
       (setq curr-det (* curr-det (car row)))
       (row-clear-leading-with
         (tuple-scalar-ndivisn row (car row))
         toclear))
     (det-recurse
       (tmp-a)
       (row-reduction
         (setq a tmp-a) #'incheck-fn #'nolead-fn))
     (incheck-fn 
       (nat-zero lead toclear)
       (when nat-zero (setq curr-det (- curr-det)))
       (det-recurse
         (nconc nat-zero
                (fix-in-lead lead toclear))))
     (nolead-fn
       (nat-zero)
       (declare (ignore nat-zero))
       (setq curr-det 0)))
    (det-recurse (if destroy a (copy-tree a)))
    ;^ First call the recursion...^
    (if a 0 curr-det)));->... then return the result.

(defun r-ef
  (a &optional (destroy nil)
     &aux (orig-mtrix (if destroy a (copy-tree a))))
  "Row-echelon form"
  (labels
    ((r-ef-recurse
       (tmp-a)
       (row-reduction
         (setq a tmp-a) #'incheck-fn #'nolead-fn))
     (incheck-fn
       (nat-zero lead toclear)
       (r-ef-recurse
         (nconc
           nat-zero
           (row-clear-leading-with
             (tuple-scalar-ndivisn lead (car lead))
             toclear))))
     (nolead-fn (nat-zero) (r-ef-recurse nat-zero)))
    (r-ef-recurse orig-mtrix)
    orig-mtrix))

(defun rr-ef
  (a &optional (destroy nil)
     &aux (also-clear nil)
     (orig-mtrix (if destroy a (copy-tree a))))
  "Reduced row-echelon form"
  (labels
    ((rr-ef-recurse
       (tmp-a)
       (row-reduction
         (setq a tmp-a) #'incheck-fn #'nolead-fn))
     (incheck-fn
       (nat-zero lead toclear)
       (tuple-scalar-ndivisn lead (car lead))
       (setq also-clear
             (cons (cdr lead)
                   (row-clear-leading-with
                     lead also-clear)))
       (rr-ef-recurse
         (nconc nat-zero
                (row-clear-leading-with
                  lead toclear))))
     (nolead-fn
       (nat-zero)
       ;V No leading 1 can be obtained, go to V
       ;V next column in /also-clear/ without V
       ;V clearing the current one.           V
       (do ((toclear also-clear (cdr toclear)))
         ((endp toclear))
         (rplaca toclear (cdar toclear)))
       (rr-ef-recurse nat-zero)))
    (rr-ef-recurse orig-mtrix)
    orig-mtrix))

(defun nullspace
  (a &key (destroy nil)
     ((dimdom dimdom-left) (length (car a)))
     &aux (also-clear nil)
     ;V Collect basis for nullspace.V
     (null-basis nil) (nullity-sofar 0))
  "A basis for the nullspace of a matrix."
  (labels
    ((add-to-basis
       ()
       (do ((toclear also-clear (cdr toclear))
            (newcol
              (nconc
                (make-list nullity-sofar
                           :initial-element 0)
                (cons -1 (make-list (decf dimdom-left)
                                    :initial-element 0)))))
         ((endp toclear)
          (incf nullity-sofar)
          (push newcol null-basis))
         (push (pop (car toclear)) newcol)))
     (nullspace-recurse
       (tmp-a)
       (row-reduction
         (setq a tmp-a) #'incheck-fn #'nolead-fn))
     (incheck-fn
       (nat-zero lead toclear)
       (tuple-scalar-ndivisn lead (car lead))
       (setq also-clear
             (cons (cdr lead)
                   (row-clear-leading-with
                     lead also-clear)))
       (decf dimdom-left)
       (nullspace-recurse
         (nconc nat-zero
                (row-clear-leading-with
                  lead toclear))))
     (nolead-fn
       (nat-zero)
       (add-to-basis)
       (nullspace-recurse nat-zero)))
    (nullspace-recurse (if destroy a (copy-tree a)))
    ;V When done with rr-ef, add all  V
    ;V remaining columns to the basis.V
    (dotimes (iters dimdom-left) (add-to-basis))
    null-basis))

(defun mtrix-square-identity (n)
  "Create a square identity matrix with /n/ rows and columns."
  (loop
    :with x = 0
    :until (= x n) :collect
    (nconc (make-list x :initial-element 0)
           (cons 1 (make-list
                     (- n (incf x))
                     :initial-element 0)))))

(defun mtrix-augment (a b)
  "Augment matrix /a/ with /b/"
  (let ((tails
          (mapcar
            (lambda (arow brow)
              (rplacd (last arow) brow))
            a b)))
    ;V Return an "undo" procedure.V
    (lambda ()
      (loop :for atail :in tails
            :do (rplacd atail nil)))))

; todo: make row swaps more efficient,
; perhaps a full-fledged row-reduction function is needed
(defun mtrix-inverse
  (a &optional (row-count (length a)))
  "Inverse of a given matrix."
  (setq a (copy-tree a))
  (mapc #'nconc a (mtrix-square-identity row-count))
  (mapcar (lambda (row) (nthcdr row-count row))
          (rr-ef a)))


