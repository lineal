
;V factorial V
;V    n!     V
(defun factorial (n)
  (declare (fixnum n))
  (loop :for prod = 1 :then (* prod i)
        :for i :from 2 :to n
        :finally (return prod)))

;V permutation V
;V     n!      V
;V  --------   V
;V  (n - r)!   V
(defun nPr (n r)
  (declare (integer n r))
  (loop :for k :from (- n r) :to n
        :for prod = 1 :then (* prod k)
        :finally (return prod)))

;V combination V
;V     n!      V
;V ----------  V
;V r!(n - r)!  V
(defun nCr (n r)
  (declare (fixnum n r))
  (/ (nPr n r) (factorial r)))

;V  assemblage  V
;V (r + n - 1)! V
;V ------------ V
;V  r!(n - 1)!  V
(defun nAr (n r)
  (declare (fixnum n r))
  (nCr (1- (+ n r)) r))

