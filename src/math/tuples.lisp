
;V Basic vector/tuple operations, V
;V they behave like the normal    V
;V lisp math functions.           V

(defun tuple-addn (&rest vecs)
  (apply #'mapcar #'+ vecs))

(defun tuple-n2addn (u v)
  (do ((u-rem u (cdr u-rem))
       (v-rem v (cdr v-rem)))
    ((or (not u-rem) (not v-rem)) u)
    (incf (car u-rem) (car v-rem))))

(defun tuple-subtrn (&rest vecs)
  (apply #'mapcar #'- vecs))

;V Take 2 vectors and destructively V
;V modify /u/ to reflect its        V
;V difference with vector /v/       V
(defun tuple-n2subtrn (u v)
  (do ((u-rem u (cdr u-rem))
       (v-rem v (cdr v-rem)))
    ((or (not u-rem) (not v-rem)) u)
    (decf (car u-rem) (car v-rem))))

(defun scalar-tuple-multn (k u)
  (mapcar (lambda (ui) (* k ui)) u))

(defun tuple-scalar-divisn (u k)
  "The vector /u/ divided by scalar /k/"
  (unless (= k 1)
    (mapcar (lambda (ui) (/ ui k)) u)))

;V Exact same as above function but  V
;V destructively modifies vector /u/.V
(defun tuple-scalar-ndivisn (u k)
  (if (= k 1) u
    (do ((tail u (cdr tail)))
      ((not tail) u)
      (rplaca tail (/ (car tail) k)))))

(defun tuple-magnitude (u)
  "Find the magnitude of a vector /u/"
  (let ((s 0))
    ;V Sum the squares of the vector's elements.V
    (mapc (lambda (ui) (incf s (expt ui 2))) u)
    (sqrt s)));-> Return the sum's square root.

;V Euclidean inner product.V
(defun dot-prod (a b)
  (loop :for x :in a
        :and y :in b
        :sum (* x y)))

;V Quick cross product, all elems V
;V passed individually for speed. V
(defun qcross3 (a b c d e f)
  (list 
    (- (* b f) (* c e))
    (- (* c d) (* a f))
    (- (* a e) (* b d))))
(defun tuple-cross3 (a b)
  (multiple-value-call
    #'qcross3 (values-list a)
    (values-list b)))

;V Projection of u onto v.V
(defun tuple-proj (u v)
  (declare (inline dot-prod))
  (scalar-tuple-multn
    (/ (dot-prod u v)
       (dot-prod v v))
    v))

;V Orthojection - orthogonal component V
;V to the projection of u onto v.      V
(defun tuple-orth (u v)
  (tuple-subtrn u (tuple-proj u v)))

