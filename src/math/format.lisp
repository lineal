
(defun max-width (&rest nums)
  (loop :for x :in nums
        :maximize (length (princ-to-string x))))

(defun output-tuple (u &optional (strm t))
  (format strm "(~{~D~#[~:;, ~]~})" u))

(defun output-matrix (mat &optional (strm t))
  (fresh-line strm)
  (loop
    :with widths = (apply #'mapcar #'max-width mat)
    :for row :in mat :do
    (princ "[" strm) :do
    (loop
      :for width :on widths
      :and elem :in row
      :if (cdr width) :do
      (format strm "~V,@A " (car width) elem)
      :else :do (format strm "~V,@A]~%" (car width) elem))))

