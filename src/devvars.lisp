
;;; Just in case.
(setq *read-default-float-format* 'double-float)

;;; Just make the package exist here,
;;; define its guts later in globals.lisp
(defpackage :lineal)

(defpackage :lineal.devvars
  (:use :cl)
  (:export *file-tree* compile-if-new compile-lineal))

(in-package :lineal.devvars)

;;; When extending lineal, add files to this list.
(defparameter *file-tree*
  '(("math" "math";<  :lineal.math
     :lineal.math "counting" "tuples"
     "matrices" "row-operns" "format")
    ("overload" "overload";<  :lineal.overload
     :lineal.overload "numbers" "tuples" "matrices")
    :lineal.overload
    ("util" "tails-do")
    ("overload" "concatenate" "crop" "row-operns" "format")
    "globals";<  :lineal
    :lineal
    "prefix-parser"
    "infix-parser"
    "save-restore"
    ("overload" :lineal.overload "client-fns")
    ))

(defun compile-if-new
  (name src-dir &key (fasl-dir src-dir)
        compile-all (load-all t) ensure-dirs
        &aux 
        (src-file
          (make-pathname :directory src-dir
                         :name name :type "lisp"))
        (fasl-file
          (make-pathname :directory fasl-dir
                         :name name :type "fasl")))
  #-sbcl (load src-file)
  ;; todo: this should be handled better,
  ;; even if loading is superfast.
  #+sbcl
  (if (and (not compile-all)
           (probe-file fasl-file)
           (< (file-write-date src-file)
              (file-write-date fasl-file)))
    (when load-all (load fasl-file))
    (progn
      (when ensure-dirs
        (ensure-directories-exist fasl-dir))
      (with-open-file
        (fasl-strm fasl-file :direction :output 
                   :if-exists :supersede)
        (compile-file src-file :output-file fasl-strm))
      (load fasl-file))))

(defun compile-tree
  (file-tree src-dir fasl-dir
             &key compile-all (load-all t)
             &aux (src-tail (last src-dir))
             (fasl-tail (last fasl-dir)))
  (labels
    ((ensure-first
       (in-dir)
       (ensure-directories-exist
         (make-pathname :directory fasl-dir))
       (let ((*package* *package*))
         (dolist (x in-dir)
           (recurse x))))
     (recurse
       (file-tree)
       (ctypecase
         file-tree
         (cons
           ;; We have a new directory to recurse.
           (let ((tmp-src-tail src-tail)
                 (tmp-fasl-tail fasl-tail)
                 (dirtail (list (car file-tree))))
             (setq src-tail (setf (cdr src-tail) dirtail)
                   fasl-tail (setf (cdr fasl-tail) dirtail))
             (ensure-first (cdr file-tree))
             (setq src-tail (rplacd tmp-src-tail nil)
                   fasl-tail (rplacd tmp-fasl-tail nil))))
         (symbol
           ;; Use a different package
           ;; for loading subsequent files.
           (setq *package* (find-package file-tree)))
         (string
           ;; /file-tree/ is a file's name,
           ;; compile it if necessary.
           (compile-if-new
             file-tree src-dir
             :fasl-dir fasl-dir
             :compile-all compile-all
             :load-all load-all)))))
    (ensure-first file-tree)))

;;; Just compile the whole program.
(defun compile-lineal (&key compile-all (load-all t))
  (compile-tree
    *file-tree* '(:relative "src")
    '(:relative "fasl")
    :compile-all compile-all
    :load-all load-all))

