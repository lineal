
(defpackage :lineal.client-vars)  

(defpackage :lineal
  (:use :cl :lineal.overload)
  (:export *saved-numbers*
           *saved-tuples* *saved-matrices*
           recall-vrbl store-vrbl
           process-input-from-stream
           process-input-from-string
           save-to-stream restore-from-stream))

(in-package :lineal)

(defparameter *saved-numbers* nil)
(defparameter *saved-tuples* nil)
(defparameter *saved-matrices* nil)

;V Get a variable out of memory.V
(defun recall-vrbl
  (k &optional (ksym (find-symbol k :lineal.client-vars)))
  (macrolet
    ((del-k-from
       (lis)
       `(setf ,lis (delete k ,lis :test #'string=))))
    (when (and ksym (boundp ksym))
      (typecase (symbol-value ksym)
        (number (del-k-from *saved-numbers*))
        (tuple (del-k-from *saved-tuples*))
        (mtrix (del-k-from *saved-matrices*)))
      (makunbound ksym))))

(defun over-recall (&rest args)
  (mapc #'recall-vrbl args)
  (format nil "Recalled: ~{~A~#[~:;, ~]~}" args))

(defun store-vrbl
  (k v &optional (ksym (intern k :lineal.client-vars)))
  (macrolet
    ((add-k-to
       (lis)
       `(setf ,lis (merge 'list ,lis (list k)
                          #'string<))))
    ;V If something's already stored V
    ;V under the name, recall it.    V
    (when (boundp ksym)
      (recall-vrbl k ksym))
    (typecase v
      (number (add-k-to *saved-numbers*))
      (tuple (add-k-to *saved-tuples*))
      (mtrix (add-k-to *saved-matrices*))))
  (set ksym v))

(defun over-store (k v)
  (if (stringp k)
    (store-vrbl k v)
    (throw
      'wtfvar
      "Store usage: (store \"[name]\" [value])
      where [name] is what you want the variable to be called
      and [value] is its desired value.")))

(store-vrbl "pi" pi)
(store-vrbl "e" (exp 1))
(store-vrbl "i" (complex 0 1))

