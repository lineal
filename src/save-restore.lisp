
(defgeneric persist-to (v strm))
(defmethod persist-to ((k number) strm)
  (format strm " ~S" k))

(defmethod persist-to ((u tuple) strm)
  (format strm " (vcat~{ ~S~})" (tuple-elems u)))

(defmethod persist-to ((a mtrix) strm)
  (format strm " (vcat~{ (cat~{ ~S~})~})"
          (mtrix-elems a)))

(defun persist-var-to (k strm)
  (format strm "(store ~S" k)
  (persist-to
    (symbol-value (find-symbol k :lineal.client-vars))
    strm)
  (princ ")" strm)
  (terpri strm))

;V Write all variables in *clent-vars* to the stream  V
;V in a special format which uses lineal's calculator V
;V syntax. For example, if we have a                  V
;V number a, vector b, and matrix c, where            V
; a = 17
; b = (1, 2, 3)
;     [1 2]
; c = [3 4]
;V the data written to the stream is                  V
; (store "a" 17)
; (store "b" (vcat 1 2 3))
; (store "c" (vcat (cat 1 2) (cat 3 4))))
;
(defun save-to-stream (strm)
  (loop :for k :in
        (append *saved-numbers* 
                *saved-tuples* *saved-matrices*)
        :do (persist-var-to k strm)))

(defun restore-from-stream (strm)
  (let ((result (process-input-from-stream strm nil)))
    (if (stringp result)
      (format nil "ERROR  ~A" result)
      "Variables successfully restored.")))

;V Save a session capture file.V
(defun local-capture
  (&optional
    (file (make-pathname :name "captured_session")))
  (with-open-file
    (strm file :direction :output
          :if-exists :supersede)
    (save-to-stream strm)))

;;; Restore from a capture file.
(defun local-restore
  (&optional
    (file (make-pathname :name "captured_session")))
  (handler-case
    (with-open-file (strm file :direction :input
                          :if-does-not-exist nil)
      (if strm (restore-from-stream strm)
        "File does not exist."))
    (error (condit)
           (declare (ignore condit))
           "Something went horribly wrong.")))

